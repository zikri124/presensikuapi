
# PresensiKuAPI

Sebuah projek untuk pemenuhan tugas mata kuliah Pemograman Web Lanjut
## Authors

Zikri Kurnia Aizet - 195150700111027
Teknologi Informasi angkatan 2019 Universitas Brawijaya


## Deployment
- Setting terlebih dahulu nama database dan JWT_SECRET pada file .env
- Tambahkan package yang dibutuhkan
  ```bash
  composer require firebase/php/jwt
  ```
- Untuk menjalankan migrasi database
  ```bash
  php artisan migrate
  ```
- Untuk menjalankan seeding terhadap satu user admin dengan email admin@admin.com dan password admin123
  ```bash
  php artisan db:seed
  ```
- Untuk menjalankan projek ini 
  ```bash
  php artisan serve
  ```
