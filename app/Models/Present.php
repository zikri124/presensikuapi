<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Present extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'time'
    ];

    protected $hidden = [
        
    ];

    public $timestamps = false;

}
