<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;

class Authenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();
        $secret = env("JWT_KEY");

        if ($token == null) {
            return response()->json([
                'success' => false,
                'message' => 'Token tidak dimasukkan'
            ], 401);
        }

        $jwt = JWT::decode($token, $secret, ['HS256']);
        $role = $jwt->role;
        
        if ($role != $guard && $guard != null) {
            return response()->json([
                'success' => false,
                'message' => 'Forbidden access'
            ], 403);
        }

        $request->merge(['name'=>$jwt->name, 'role'=>$role]);
        return $next($request);
    }
}
