<?php

namespace App\Http\Controllers;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'pin' => ['required']
        ]);

        if ($validation->fails()) {
            return response()->json([
                'message' => 'Inputan kamu tidak sesuai',
                'error' => $validation->errors()
            ], 403);
        }

        $emailExists = User::where('email', $request->email)->first();

        if ($emailExists == null) {
            $hashedPS = Hash::make($request->password);
            $hashedPIN = Hash::make($request->pin);

            $user = User::create(['name' => $request->name, 'email' => $request->email, 'password' => $hashedPS, 'pin' => $hashedPIN]);

            if ($user) {
                $jwt = JWT::encode(
                    [
                        "sub" => $user->email,
                        "name" => $user->name,
                        "iat" => time(),
                        "uat" => time(),
                        "role" => 'user',
                    ],
                    env('JWT_KEY', env('JWT_KEY')),
                    'HS256'
                );
                return response()->json([
                    'message' => 'Berhasi daftar',
                    'token' => $jwt,
                    'user' => $user
                ], 201);
            }
            return response()->json([
                'message' => 'Error ketika mendaftar'
            ], 409);
        } else {
            return response()->json([
                'message' => 'Email sudah terdaftar'
            ], 409);
        }
    }

    public function login(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (!$validation) {
            return response()->json([
                'message' => 'Inputan kamu tidak sesuai',
                'error' => $validation->errors()
            ], 403);
        }

        $user = User::where('email', $request->email)->first();
        
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $jwt = JWT::encode(
                    [
                        "sub" => $user->email,
                        "name" => $user->name,
                        "iat" => time(),
                        "role" => $user->role,
                    ],
                    env('JWT_KEY'),
                    'HS256'
                );
                return response()->json([
                    'message' => 'Berhasil login',
                    'token' => $jwt,
                    'user' => $user
                ], 200);
            }
            return response()->json([
                'message' => 'Password yang dimasukkan salah'
            ], 401);
        } else {
            return response()->json([
                'message' => 'User tidak ditemukan'
            ], 404);
        }
    }

    public function showUserByID($userId)
    {
        $userFind = User::where('id', $userId)->first();
        if (!$userFind) {
            return response()->json([
                'message' => 'User tidak ditemukan'
            ], 404);
        }
        return response()->json([
            'message' => 'User ditemukan',
            'user' => $userFind
        ], 200);
    }

    public function showAllUser()
    {
        $userFind = User::all();
        return response()->json([
            'message' => 'User ditemukan',
            'users' => $userFind
        ], 200);
    }

    public function editUser(Request $request, $userId)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required'],
            'password' => ['required'],
            'pin' => ['required'],
            'role' => ['required']
        ]);

        if ($validation->fails()) {
            return response()->json([
                'message' => 'Inputan kamu tidak sesuai',
                'error' => $validation->errors()
            ], 403);
        }

        $userFind = User::where('id', $userId)->first();
        if (!$userFind) {
            return response()->json([
                'message' => 'User tidak ditemukan'
            ], 404);
        }

        $hashedPS = Hash::make($request->password);
        $hashedPIN = Hash::make($request->pin);

        $userFind->name = $request->name;
        $userFind->password = $hashedPS;
        $userFind->pin = $hashedPIN;
        $userFind->role = $request->role;
        $userFind->save;

        $jwt = JWT::encode(
            [
                "sub" => $userFind->email,
                "name" => $userFind->name,
                "iat" => time(),
                "role" => $userFind->role,
            ],
            env('JWT_KEY', env('JWT_KEY')),
            'HS256'
        );

        return response()->json([
            'message' => 'Berhasil mengedit user',
            'token' => $jwt,
            'user' => $userFind
        ], 200);
    }

    public function delUser($userId)
    {
        User::destroy($userId);
        return response()->json([
            'message' => 'Berhasil menghapus user'
        ], 200);
    }
}
