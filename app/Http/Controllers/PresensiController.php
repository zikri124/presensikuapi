<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Present;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PresensiController extends Controller
{
    public function getAllPresent(Request $request)
    {
        $role = $request->role;

        if ($role == 'admin') {
            $presents = Present::all();
        } else {
            $userFind = User::where('name', $request->name)->first();
            $presents = Present::where('user_id', $userFind->id)->get();
        }
        return response()->json([
            'presents' => $presents
        ], 200);
    }
    
    public function getAllPresentFromAnUser($userId)
    {
        $userFind = User::where('id', $userId)->first();
        if ($userFind) {
            $presents = Present::where('user_id', $userId)->get();
            return response()->json([
                'message' => 'Berhasil mendapatkan daftar presensi',
                'presents' => $presents
            ], 200);
        } else {
            return response()->json([
                'message' => 'User tidak ditemukan'
            ], 404);
        }
    }

    public function addPresent(Request $request)
    {
        $userId = $request->userId;
        $userFind = User::where('id', $userId)->first();
        if (!$userFind) {
            return response()->json([
                'message' => 'User tidak ditemukan'
            ], 404);
        } 

        $pin = $request->pin;
        if (!Hash::check($pin, $userFind->pin)) {
            return response()->json([
                'message' => 'PIN yang dimasukkan salah'
            ], 401);
        }

        $present = new Present;
        $present->userId = $userId;
        $present->save();

        return response()->json([
            'message' => 'Berhasil presensi',
            'user' => $present
        ], 201);
    }

    public function delPresent($presentId)
    {
        Present::destroy($presentId);
        return response()->json([
            'message' => 'Berhasil menghapus presensi'
        ], 200);
    }
}
