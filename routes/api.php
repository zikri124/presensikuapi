<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post(
    '/login',
    'App\Http\Controllers\UserController@login'
);

Route::middleware('auth:admin')->get(
    '/present',
    'App\Http\Controllers\PresensiController@getAllPresent'
);

Route::prefix('user')->group(function () {
    Route::middleware('auth:admin')->group(function () {
        Route::get(
            '/list',
            'App\Http\Controllers\UserController@showAllUser'
        );
        Route::get(
            '/{userId}',
            'App\Http\Controllers\UserController@showUserByID'
        );
        Route::post(
            '/adduser',
            'App\Http\Controllers\UserController@register'
        );
        Route::put(
            '/{userId}',
            'App\Http\Controllers\UserController@editUser'
        );
        Route::delete(
            '/{userId}',
            'App\Http\Controllers\UserController@delUser'
        );
    });
});

Route::prefix('present')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::post(
            '/new',
            'App\Http\Controllers\PresensiController@addPresent'
        );
        Route::get(
            '/',
            'App\Http\Controllers\PresensiController@getAllPresent'
        );
    });
    Route::middleware('auth:admin')->group(function () {
        Route::delete(
            '/{presentId}',
            'App\Http\Controllers\PresensiController@delPresent'
        );
        Route::get(
            '/{userId}',
            'App\Http\Controllers\PresensiController@getAllPresentFromAnUser'
        );
    });
});
