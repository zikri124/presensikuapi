<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PresensiController;

Route::get('/', function () {
    return "Server Online";
});

Route::group(['prefix' => 'user'], function () {
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/all', [
            UserController::class, 'showAllUser'
        ]);

        Route::post('/register', [
            UserController::class, 'register'
        ]);

        Route::put('/{userId}', [
            UserController::class, 'editUser'
        ]);

        Route::delete('/{userId}', [
            UserController::class, 'delUser'
        ]);
    });

    Route::post('/login', [
        UserController::class, 'login'
    ]);

    Route::get('/{userId}', [
        UserController::class, 'showUserByID'
    ]);
});

Route::prefix('present')->group(function () {
    Route::middleware('auth:admin')->group(function () {
        Route::delete('/{presentId}', [
            PresensiController::class, 'delPresent'
        ]);

        Route::get('/', [
            PresensiController::class, 'getAllPresent'
        ]);
    });

    Route::middleware('auth')->group(function () {
        Route::post('/new', [
            PresensiController::class, 'addPresent'
        ]);

        Route::get('/{userId}', [
            PresensiController::class, 'getAllPresentFromAnUser'
        ]);
    });
});
